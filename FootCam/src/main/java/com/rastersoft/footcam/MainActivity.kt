/*
 * Copyright 2022 (C) Raster Software Vigo (Sergio Costas)
 *
 * This file is part of FootCam
 *
 * FootCam is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * FootCam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.rastersoft.footcam

import android.Manifest
import android.content.ContentValues
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.provider.MediaStore
import android.util.Log
import android.util.Size
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.rastersoft.footcam.databinding.ActivityMainBinding
import java.io.InputStream
import java.io.OutputStream
import java.net.Socket
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {
    private lateinit var viewBinding: ActivityMainBinding

    private var imageCapture: ImageCapture? = null
    private var cameraEnabled: Boolean = false
    private var doTakePicture: Boolean = false
    private var preview: Preview? = null
    private var cameraProvider: ProcessCameraProvider? = null
    private var cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
    private var countdown : CountDownTimer? = null
    private var cameraExecutor: ExecutorService? = null
    private var timeOutValue: Long = 25000

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        when (keyCode) {
            KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP -> {
                enableCamera(true)
                return true
            }
            KeyEvent.KEYCODE_BACK -> {
                disableCamera()
                finish()
                return true
            }
        }
        return false
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        when (keyCode) {
            KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP, KeyEvent.KEYCODE_BACK -> {
                return true
            }
        }
        return false
    }

    private fun clicked() {
        enableCamera(false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        supportActionBar?.hide()
        // Request camera permissions
        while (!allPermissionsGranted()) {
            ActivityCompat.requestPermissions(
                this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS
            )
        }

        viewBinding.viewFinder.setOnClickListener { clicked() }
        viewBinding.editTextIP.setOnKeyListener(object: View.OnKeyListener {
            override fun onKey(view: View?, keyCode: Int, keyEvent: KeyEvent?): Boolean {
                when (keyCode) {
                    KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP, KeyEvent.KEYCODE_BACK -> {
                        return false
                    }
                }
                updateIpValues()
                return false
            }
        })
        viewBinding.editTextPortNumber.setOnKeyListener(object: View.OnKeyListener {
            override fun onKey(view: View?, keyCode: Int, keyEvent: KeyEvent?): Boolean {
                when (keyCode) {
                    KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP, KeyEvent.KEYCODE_BACK -> {
                        return false
                    }
                }
                updateIpValues()
                return false
            }
        })
        viewBinding.editTextResolution.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(view: View?, keyCode: Int, keyEvent: KeyEvent?): Boolean {
                when (keyCode) {
                    KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP, KeyEvent.KEYCODE_BACK -> {
                        return false
                    }
                }
                disableCamera()
                return false
            }
        })
        updateNotification()
    }

    fun updateIpValues() {
        if (checkIpAddress()) {
            viewBinding.editTextIP.setTextColor(Color.parseColor("#FF000000"))
        } else {
            viewBinding.editTextIP.setTextColor(Color.parseColor("#FFFF0000"))
        }
        if (checkIpPort()) {
            viewBinding.editTextPortNumber.setTextColor(Color.parseColor("#FF000000"))
        } else {
            viewBinding.editTextPortNumber.setTextColor(Color.parseColor("#FFFF0000"))
        }
    }

    private fun checkIpAddress(): Boolean {
        val values = viewBinding.editTextIP.text.split(".")
        if (values.size != 4) {
            return false
        }
        for (strItem in values) {
            try {
                val item = Integer.parseInt(strItem)
                if ((item > 255) || (item < 0)) {
                    return false
                }
            } catch (e: Exception) {
                return false
            }
        }
        return true
    }

    private fun checkIpPort(): Boolean {
        val port : Int
        try {
            port = Integer.parseInt(viewBinding.editTextPortNumber.text.toString())
        } catch (e: Exception) {
            return false
        }
        if ((port > 65535) || (port < 1)) {
            return false
        }
        return true
    }

    override fun onResume() {
        super.onResume()
        val preferences = this.getPreferences(Context.MODE_PRIVATE)
        try {
            viewBinding.editTextIP.setText(preferences.getString("ip_address", ""))
            viewBinding.editTextPortNumber.setText(preferences.getInt("ip_port", 9000).toString())
            viewBinding.editTextResolution.setText(preferences.getInt("resolution", 0).toString())
        } catch (exc: java.lang.Exception) {}
        updateIpValues()
    }

    override fun onPause() {
        super.onPause()
        val preferences = this.getPreferences(Context.MODE_PRIVATE).edit()
        preferences.putString("ip_address", viewBinding.editTextIP.text.toString())
        preferences.putInt("ip_port", Integer.parseInt(viewBinding.editTextPortNumber.text.toString()))
        preferences.putInt("resolution", Integer.parseInt(viewBinding.editTextResolution.text.toString()))
        preferences.apply()
    }

    private fun updateNotification() {
        if (cameraEnabled) {
            viewBinding.notification.setText(R.string.camera_enabled)
        } else {
            viewBinding.notification.setText(R.string.camera_disabled)
        }
    }

    private fun setTimeout() {
        countdown?.cancel()
        countdown = object:CountDownTimer(timeOutValue, 1000) {
            override fun onTick(millisUntilFinished: Long) {
            }
            override fun onFinish() {
                disableCamera()
            }
        }.start()
    }

    private fun enableCamera(takeAPicture: Boolean) {
        Log.d("Cosas", "Estado: $cameraEnabled")
        if (!cameraEnabled) {
            viewBinding.viewFinder.setBackgroundColor(Color.parseColor("#FF000000"))
            viewBinding.viewFinder.invalidate()
            doTakePicture = takeAPicture
            try {
                cameraProvider?.unbindAll()
            } catch (exc: java.lang.Exception) {
            }

            startCamera()
            cameraExecutor = Executors.newSingleThreadExecutor()
            cameraEnabled = true
        } else {
            setTimeout()
            if (takeAPicture) {
                takePhoto()
            }
        }
        updateNotification()
    }

    private fun disableCamera() {
        Log.d("Cosas", "Disabling camera $cameraEnabled")
        countdown?.cancel()
        countdown = null
        if (cameraEnabled) {
            try {
                // Unbind use cases before rebinding
                cameraProvider?.unbindAll()
            } catch (exc: java.lang.Exception) {
            }
            cameraExecutor?.shutdown()
            cameraEnabled = false
            doTakePicture = false
        }
        updateNotification()
    }
    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            baseContext, it) == PackageManager.PERMISSION_GRANTED
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraExecutor?.shutdown()
    }

    companion object {
        private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"
        private const val REQUEST_CODE_PERMISSIONS = 10
        private val REQUIRED_PERMISSIONS =
            mutableListOf (
                Manifest.permission.CAMERA
            ).apply {
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
                    add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                }
            }.toTypedArray()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults:
        IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startCamera()
            } else {
                Toast.makeText(this,
                    "Permissions not granted by the user.",
                    Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)

        cameraProviderFuture.addListener({
            // Used to bind the lifecycle of cameras to the lifecycle owner
            cameraProvider = cameraProviderFuture.get()

            // Preview
            preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(viewBinding.viewFinder.surfaceProvider)
                }
            var resolutionStr = viewBinding.editTextResolution.text.toString()
            if (resolutionStr == "") {
                resolutionStr = "0"
            }
            val resolution = Integer.parseInt(resolutionStr)
            if (resolution != 0) {
                val aspectRatio = viewBinding.viewFinder.width.toDouble() / viewBinding.viewFinder.height.toDouble()
                val resX : Int
                val resY : Int
                if (aspectRatio > 1) {
                    resX = resolution
                    resY = (resolution.toDouble() / aspectRatio).toInt()
                } else {
                    resX = (resolution.toDouble() * aspectRatio).toInt()
                    resY = resolution
                }
                imageCapture = ImageCapture.Builder().setTargetResolution(Size(resX, resY)).build()
            } else {
                imageCapture = ImageCapture.Builder().build()
            }

            // Select back camera as a default

            try {
                // Unbind use cases before rebinding
                cameraProvider?.unbindAll()
                // Bind use cases to camera
                cameraProvider?.bindToLifecycle(
                    this, cameraSelector, preview)
                cameraProvider?.bindToLifecycle(
                    this,cameraSelector, imageCapture)
                setTimeout()
                if (doTakePicture) {
                    takePhoto()
                    doTakePicture = false
                }
            } catch(exc: Exception) {
                Log.e("Mycamera", "Use case binding failed", exc)
            }
        }, ContextCompat.getMainExecutor(this))

    }

    private fun takePhoto() {
        // Get a stable reference of the modifiable image capture use case
        val imageCapture = imageCapture ?: return

        val name = SimpleDateFormat(FILENAME_FORMAT, Locale.US)
            .format(System.currentTimeMillis())

        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, name)
            put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
            if(Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/CameraX-Image")
            }
        }

        // Create output options object which contains file + metadata
        val outputOptions = ImageCapture.OutputFileOptions
            .Builder(contentResolver,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                contentValues)
            .build()
        imageCapture.takePicture(
            outputOptions,
            ContextCompat.getMainExecutor(this),
            object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Log.e("Mycamera", "Photo capture failed: ${exc.message}", exc)
                }

                override fun
                        onImageSaved(output: ImageCapture.OutputFileResults){

                    val msg = "Photo capture succeeded: ${output.savedUri}"
                    Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
                    sendPicture(output.savedUri)
                }
            }
        )
    }

    private fun sendPicture(uri: Uri?) {
        if (uri == null) {
            return
        }
        if (!checkIpAddress() || !checkIpPort()) {
            return
        }
        val thread = Thread(object: Runnable {
            override fun run() {
                var sock: Socket? = null
                var data: OutputStream? = null
                var file: InputStream? = null
                try {
                    sock = Socket(viewBinding.editTextIP.text.toString(), Integer.parseInt(viewBinding.editTextPortNumber.text.toString()))
                    data = sock.getOutputStream()
                    file = contentResolver.openInputStream(uri)
                    data.write(file?.readBytes())
                } catch (exc: Exception) {
                }
                file?.close()
                data?.close()
                sock?.close()
            }
        })
        thread.start()
    }
}
