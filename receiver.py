#!/usr/bin/env python3

import io
import socket
import wx

class Visor(wx.Frame):
    """ class Panel1 creates a panel with an image on it, inherits wx.Panel """
    def __init__(self):
        # create the panel
        wx.Frame.__init__(self, None)
        self.Maximize(True)
        self.panel = wx.Panel(self, -1)

        self._serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._serversocket.settimeout(0.1)
        self._serversocket.bind(('', 9000))
        self._serversocket.listen(5)

        self.timer1 = wx.Timer(self, 1)
        self.Bind(wx.EVT_TIMER, self.OnTimer)
        self.Show(1)
        self.timer1.StartOnce(500)

    def OnTimer(self, event):

        (w,h) = self.GetSize()
        data = b""
        # accept connections from outside
        try:
            (clientsocket, address) = self._serversocket.accept()
        except:
            self.timer1.StartOnce(500)
            return
        while True:
            recv_data = clientsocket.recv(1000000)
            if len(recv_data) == 0:
                clientsocket.close()
                break
            data += recv_data
        try:
            st = io.BytesIO(data)
            handler = wx.JPEGHandler()
            image = wx.Image()
            handler.LoadFile(image, st, True, 0)
            image = image.Scale(w, h, wx.IMAGE_QUALITY_HIGH).ConvertToBitmap()

            # bitmap upper left corner is in the position tuple (x, y) = (5, 5)
            wx.StaticBitmap(self.panel, -1, image, (0, 0), (image.GetWidth(), image.GetHeight()))
        except IOError:
            pass
        print("Showing")
        self.timer1.StartOnce(500)


picture = None
app = wx.App()
frame1 = Visor()

app.MainLoop()
