# FootCam

![logo](FootCam.png)

A remote-controlled camera for Android

## Why another camera application?

I love to make electronics projects, but documenting them is hard because
each time that I want to take a picture, I have to stop to take the phone,
unlock it, frame what I'm doing...

I then decided to use a tripod to hold my phone, and use a foot-activated
pushbutton (which I explain how to build later) to trigger the camera.
Unfortunately, the native camera application closes itself automatically after
about two minutes, no matter that the screen lock is disabled.

I decided then to write a simple camera application that just keeps itself
opened all the time. Unfortunately, it seems that the reason to disable the
camera after that time is to avoid overheating... so I added a timeout to my
application too, which just disables the camera and the preview, but the app
remains open, so pressing the foot pedal will enable the camera and shoot
a picture, and after the timeout the camera will be disabled again. It also
prevents the screen lock to be triggered, so there is no need to manually
disable it before the session.

But also I found that I couldn't know in advance if the picture was right, so
I added an extra function: send each new picture over the local network to my
laptop, where a simple program would show it on the screen and allow me to
decide whether to keep it or take another one.

## Downloading FootCam

At the "TAGS" section you can get a .zip or .tar.gz file with the source code,
or an .APK file with the compiled code, ready to be installed in your phone.

You can download the *receiver.py* file with the "laptop visualizer" directly
from gitlab: just click on the file and choose the "Download" icon (top-right
corner).

## Building the foot pedal

It is possible to connect a button to the headphones connector to control the
volume, and this is exactly what I did. The Google official specification for
the headphones connector can be found at:

[Android jack accessories specification](https://source.android.com/docs/core/interaction/accessories/headset/plug-headset-spec?hl=en)

[Android jack device specification](https://source.android.com/docs/core/interaction/accessories/headset/plug-headset-spec?hl=en)

Although the specification looks simple, unfortunately there is a detail that
is not very clear: the microphone impedance. According to the specification,
the microphone must have a DC impedance of "1000 ohms or higher", but it
doesn't say a maximum value, so an "infinite" value could seem valid.
Unfortunately that's not the case: you can't just leave the pins 3 and 4
unconnected while no button is pressed, or it won't work (or, at least, in my
phone it won't work). That's why the circuit must be a little bit more complex.

This is the schematic for the circuit:

![Foot pedal schematic](schematic.png)

And this is the prototype that I built:

![A picture of the prototype](pedal.jpg)

I added a second, empty board to cover the solder face, and put the buttons
facing down. And this is the result:

![Final](foot.jpg)

## Using FootCam

After installing the APK or compiling and loading it with AndroidStudio, just
launch it. You will see something like this:

![Screen capture of the app](footcam.jpg)

Touching on the screen will activate the camera for 15 seconds. You can use
this to frame your workbench and prepare everything for the session. When it
is ready, just begin to work and whenever you want to take a picture, press the
footpedal. It will require about two to three seconds to take it because it
needs to enable the camera (if it was disabled) and focus.

### Remote view

If you want to see the picture that you took, you can launch the program

    receiver.py

in a laptop. It is written in Python3 and Wxwidgets, so you will have to
download and install both if your computer doesn't have them installed (usually
you only need to do that on Windows; Linux should have it by default).

After installing and launching it, just type the IP address of the laptop on
the text entry... et voila! Every time that you take a picture, FootCam will
send it to the laptop and you will be able to decide whether to keep it or to
take another one.

## License

FootCam is distributed under the GPLv3 license. You have the full text in the
COPYING file.

## About

FootCam has been created by Raster Software Vigo (Sergio Costas)

<https://gitlab.com/rastersoft/footcam>  
<https://www.rastersoft.com>  
rastersoft@gmail.com
